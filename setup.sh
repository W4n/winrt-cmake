#!/bin/bash

set -eu

mkdir -p _build
cd _build/
cmake   -G "Visual Studio 15 2017 Win64"\
        -T "host=x64" \
        -DCMAKE_SYSTEM_NAME=WindowsStore -DCMAKE_SYSTEM_VERSION=10.0 ..
cd -
