# cmake-winrt

Reproduction of a CMake issue with Visual Studio 2017.

To replicate the bug, run:

```
./setup.sh
```

Then open the solution in `_build/Project.sln` and build projects.

## Issue

winrt-lib2 depends on winrt-lib1 but it fails at compile time.